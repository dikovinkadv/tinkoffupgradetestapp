package myupgradeapplication

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import myupgradeapplication.pages.MainPage
import myupgradeapplication.pages.LoginPage
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R

class NavigationTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun textPositionNotSavedTest() {
        val mainPage = MainPage()
        with(mainPage){
            pressChangeButton()
            checkTextOnPage(SECOND_TEXT)
            pressNextButton()
        }
        with(LoginPage()) {
            pressPreviousButton()
        }
        with(mainPage){
            checkTextOnPage(FIRST_TEXT)
        }
    }
    @Test
    fun testLoginPasswordValuesNotSaved() {
        val loginValue = "Dikovinka"
        val passwordValue = "Parolchik"
        val mainPage = MainPage()
        val loginPage = LoginPage()
        with(mainPage){
            pressNextButton()
        }
        with(loginPage) {
            enterLogin(loginValue)
            enterPassword(passwordValue)
            pressPreviousButton()
        }
        with(mainPage){
            pressNextButton()
        }
        with(loginPage) {
            checkEmptyLoginField()
            checkEmptyPasswordField()
        }
    }

    private companion object {
        val FIRST_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.first_text)
        val SECOND_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.second_text)
    }
}
