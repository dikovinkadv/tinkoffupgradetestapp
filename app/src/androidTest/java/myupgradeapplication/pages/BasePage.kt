package myupgradeapplication.pages

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice

open class BasePage {
    val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    var waitingTimeOut = 2000L
}
