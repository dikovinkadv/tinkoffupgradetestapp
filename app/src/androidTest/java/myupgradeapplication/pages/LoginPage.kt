package myupgradeapplication.pages

import org.junit.Assert.assertEquals
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiObject2
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.R

class LoginPage : BasePage() {
    private val loginFiledSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/edittext_login")
    private val passwordFiledSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/edittext_password")
    private val submitButtonSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/button_submit")
    private val previousButtonSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/button_second")

    fun pressPreviousButton() {
        device
            .wait(Until.findObject(previousButtonSelector), waitingTimeOut)
            .click()
    }
    fun enterLogin(loginValue: String) {
        val loginField = getLoginFieldUiObject()
        loginField?.text = loginValue

    }
    fun enterPassword(passwordValue: String) {
        device
            .wait(Until.findObject(passwordFiledSelector), waitingTimeOut)
            .text = passwordValue
    }

    fun pressSubmitButton() {
        device
            .wait(Until.findObject(submitButtonSelector), waitingTimeOut)
            .click()
    }

    fun checkTextOnSnackBar(text: String) {
        assert(
            device.wait(Until.hasObject(By.text(text)), waitingTimeOut))
    }

    fun checkEmptyLoginField() {
        val loginField = getLoginFieldUiObject()
        assertEquals(
            LOGIN_FIELD_HINT_TEXT, loginField?.text)
    }

    fun checkEmptyPasswordField() {
        val passwordField = device
            .wait(Until.findObject(passwordFiledSelector), waitingTimeOut)
        assertEquals(
            PASSWORD_FIELD_HINT_TEXT, passwordField.text)
    }

    private fun getLoginFieldUiObject(): UiObject2? {
        return device
            .wait(Until.findObject(loginFiledSelector), waitingTimeOut)
    }

    private companion object {
        val LOGIN_FIELD_HINT_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.login_hint)
        val PASSWORD_FIELD_HINT_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.password_hint)
    }
}
