package myupgradeapplication.pages

import org.junit.Assert.assertTrue
import org.junit.Assert.assertEquals
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until

class MainPage : BasePage() {
    private val nextButtonSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/button_first")
    private val changeButtonSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/change_button")
    private val showDialogSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/dialog_button")
    private val titleSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/alertTitle")
    private val messageSelector = By.res(
        "android:id/message")
    private val textSelector = By.res(
        "ru.tinkoff.myupgradeapplication:id/textview_first")

    fun pressNextButton() {
        device
            .wait(Until.findObject(nextButtonSelector), waitingTimeOut)
            .click()
    }

    fun pressChangeButton() {
        device
            .wait(Until.findObject(changeButtonSelector), waitingTimeOut)
            .click()
    }

    fun checkTextOnPage(textOnPage: String) {
        val dialogText = device
            .wait(Until.findObject(textSelector), waitingTimeOut)
        assertEquals(
            textOnPage, dialogText.text)
    }

    fun pressShowDialogButton() {
        device
            .wait(Until.findObject(showDialogSelector), waitingTimeOut)
            .click()
    }

    fun checkDialogTitle(dialogTitle: String) {
        val titleDialog = device
            .wait(Until.findObject(titleSelector), waitingTimeOut)
        assertEquals(
            dialogTitle, titleDialog.text)
    }

    fun checkDialogMessage(dialogMessage: String) {
        val messageDialog = device
            .wait(Until.findObject(messageSelector), waitingTimeOut)
        assertEquals(
            dialogMessage, messageDialog.text)
    }

    fun checkDialogIsClosed() {
        val dialogErrorMessage = "Диалоговое окно должно быть закрыто"
        val titleDialogGone =
            device.wait(Until.gone(titleSelector), waitingTimeOut)
        val messageDialogGone =
            device.wait(Until.gone(messageSelector), waitingTimeOut)
        assertTrue(
            dialogErrorMessage, titleDialogGone && messageDialogGone)
    }
}
