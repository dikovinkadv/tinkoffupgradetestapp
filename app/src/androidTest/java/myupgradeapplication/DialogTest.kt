package myupgradeapplication

import androidx.test.ext.junit.rules.ActivityScenarioRule
import myupgradeapplication.pages.MainPage
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity

class DialogTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun showDialogTest() {
        with(MainPage()){
            pressShowDialogButton()
            checkDialogTitle(DIALOG_TITLE)
            checkDialogMessage(DIALOG_MESSAGE)
        }
    }

    @Test
    fun closeDialogTest() {
        with(MainPage()){
            pressShowDialogButton()
            checkDialogTitle(DIALOG_TITLE)
            checkDialogMessage(DIALOG_MESSAGE)
            device.pressBack()
            checkDialogIsClosed()
        }
    }

    private companion object {
        val DIALOG_TITLE = "Важное сообщение"
        val DIALOG_MESSAGE = "Теперь ты автоматизатор"
    }
}
