package myupgradeapplication.kaspresso

import org.junit.Test
import org.junit.Rule
import myupgradeapplication.kaspresso.pages.MainPage
import myupgradeapplication.kaspresso.pages.LoginPage
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import ru.tinkoff.myupgradeapplication.MainActivity

class DisplayErrorMessagesTest : TestCase() {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun passwordFieldMustBeFilledMessageTest() =
        run {
            val loginValue = "Dikovinka"
            val textOnSnackBar = "Password field must be filled!"
            step("Нажать на кнопку Next") {
                MainPage {
                    nextButton.click()
                }
            }
            LoginPage {
                step("Ввести значение в поле login") {
                    loginField.typeText(loginValue)
                }
                step("Нажать submit") {
                    submitButton.click()
                }
                step("Выводится сообщение $textOnSnackBar") {
                    snackBarText.hasText(textOnSnackBar)
                }
            }
        }

    @Test
    fun loginFieldMustBeFilledMessageTest() =
        run {
            val passwordValue = "Parolchik"
            val textOnSnackBar = "Login field must be filled!"
            step("Нажать на кнопку Next") {
                MainPage {
                    nextButton.click()
                }
            }
            LoginPage {
                step("Ввести значение в поле password") {
                    passwordFiled.typeText(passwordValue)
                }
                step("Нажать submit") {
                    submitButton.click()
                }
                step("Выводится сообщение $textOnSnackBar") {
                    snackBarText.hasText(textOnSnackBar)
                }
            }
        }

    @Test
    fun bothOfFieldsMustBeFilledMessageTest() =
        run {
            val textOnSnackBar = "Both of fields must be filled!"
            step("Нажать на кнопку Next") {
                MainPage {
                    nextButton.click()
                }
            }
            LoginPage {
                step("Нажать submit") {
                    submitButton.click()
                }
                step("Выводится сообщение $textOnSnackBar") {
                    snackBarText.hasText(textOnSnackBar)
                }
            }
        }
}
