package myupgradeapplication.kaspresso

import org.junit.Test
import org.junit.Rule
import myupgradeapplication.kaspresso.pages.MainPage
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import ru.tinkoff.myupgradeapplication.MainActivity

class DialogTest : TestCase() {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun showDialogTest() =
        run {
            MainPage {
                step("Нажать на кнопку Show Dialog") {
                    showDialog.click()
                }
                step("Диалог окно с заголовком $DIALOG_TITLE и текстом $DIALOG_MESSAGE") {
                    dialogTitle.hasText(DIALOG_TITLE)
                    dialogMessage.hasText(DIALOG_MESSAGE)
                }
            }
        }

    @Test
    fun closeDialogTest() =
        run {
            MainPage {
                step("Нажать на кнопку Show Dialog") {
                    showDialog.click()
                }
                step("Диалог окно с заголовком $DIALOG_TITLE и текстом $DIALOG_MESSAGE") {
                    dialogTitle.hasText(DIALOG_TITLE)
                    dialogMessage.hasText(DIALOG_MESSAGE)
                }
                step("Нажать системную кнопку “Назад”") {
                    pressBack()
                }
                step("Диалогвое окно с заголовком и текстом не отображается") {
                    dialogTitle.doesNotExist()
                    dialogMessage.doesNotExist()
                }
            }
        }

    private companion object {
        const val DIALOG_TITLE = "Важное сообщение"
        const val DIALOG_MESSAGE = "Теперь ты автоматизатор"
    }
}
