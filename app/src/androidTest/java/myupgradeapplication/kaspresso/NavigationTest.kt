package myupgradeapplication.kaspresso

import org.junit.Test
import org.junit.Rule
import androidx.test.ext.junit.rules.ActivityScenarioRule
import myupgradeapplication.kaspresso.pages.LoginPage
import myupgradeapplication.kaspresso.pages.MainPage
import ru.tinkoff.myupgradeapplication.MainActivity
import androidx.test.platform.app.InstrumentationRegistry
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import ru.tinkoff.myupgradeapplication.R

class NavigationTest : TestCase() {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun textPositionNotSavedTest() =
        run {
            MainPage {
                step("Нажать на кнопку Change") {
                    changeButton.click()
                }
                step("Проверка смены текста") {
                    textOnPage.hasText(SECOND_TEXT)
                }
                step("Нажать на кнопку Next") {
                    nextButton.click()
                }
            }
            step("Нажать на кнопку Previous") {
                LoginPage {
                    previousButton.click()
                }
            }
            step("В текстовом поле отображается текст по умолчанию") {
                MainPage {
                    textOnPage.hasText(FIRST_TEXT)
                }
            }
        }

    @Test
    fun testLoginPasswordValuesNotSaved() =
        run {
            val loginValue = "Dikovinka"
            val passwordValue = "Parolchik"
            step("Нажать на кнопку Next") {
                MainPage {
                    nextButton.click()
                }
            }
            LoginPage {
                step("Заполнить поля Login, Password") {
                    loginField.typeText(loginValue)
                    passwordFiled.typeText(passwordValue)
                }
                step("Нажать на кнопку Previous") {
                    previousButton.click()
                }
            }
            step("Нажать на кнопку Next") {
                MainPage {
                    nextButton.click()
                }
            }
            step("Поля login и password пустые") {
                LoginPage {
                    loginField.hasEmptyText()
                    passwordFiled.hasEmptyText()
                }
            }
        }

    private companion object {
        val FIRST_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.first_text)
        val SECOND_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.second_text)
    }
}
