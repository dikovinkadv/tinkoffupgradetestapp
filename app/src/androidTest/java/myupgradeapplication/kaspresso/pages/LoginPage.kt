package myupgradeapplication.kaspresso.pages

import android.widget.LinearLayout
import com.google.android.material.textview.MaterialTextView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R

class LoginPage : BasePage() {
    val previousButton = KButton { withId(R.id.button_second) }
    val submitButton = KButton { withId(R.id.button_submit) }
    val loginField = KEditText { withId(R.id.edittext_login) }
    val passwordFiled = KEditText { withId(R.id.edittext_password) }
    val snackBarText = KTextView {
        isInstanceOf(MaterialTextView::class.java)
        withParent { isInstanceOf(LinearLayout::class.java) }
    }

    companion object {
        inline operator fun invoke(crossinline block: LoginPage.() -> Unit) =
            LoginPage().block()
    }
}
