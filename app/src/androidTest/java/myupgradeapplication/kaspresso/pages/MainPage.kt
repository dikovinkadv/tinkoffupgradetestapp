package myupgradeapplication.kaspresso.pages

import android.widget.LinearLayout
import androidx.appcompat.widget.DialogTitle
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R

class MainPage : BasePage() {
    val nextButton = KButton { withId(R.id.button_first) }
    val changeButton = KButton { withId(R.id.change_button) }
    val showDialog = KButton { withId(R.id.dialog_button) }
    val dialogTitle = KTextView {
        isInstanceOf(DialogTitle::class.java)
        withParent { isInstanceOf(LinearLayout::class.java) }
    }
    val dialogMessage = KTextView { withId(android.R.id.message) }
    val textOnPage = KTextView { withId(R.id.textview_first) }

    companion object {
        inline operator fun invoke(crossinline block: MainPage.() -> Unit) =
            MainPage().block()
    }
}
