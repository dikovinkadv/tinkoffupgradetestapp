package myupgradeapplication.espresso

import androidx.test.espresso.Espresso.pressBack
import org.junit.Test
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.junit.rules.ActivityScenarioRule
import ru.tinkoff.myupgradeapplication.MainActivity
import myupgradeapplication.espresso.pages.MainPage

@RunWith(AndroidJUnit4::class)
class DialogTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun showDialogTest() {
        with(MainPage()) {
            pressShowDialogButton() //Нажать на кнопку Show Dialog
            checkDialogTitleIsDisplayed(DIALOG_TITLE) //ОР: Диалоговое окно,тайтл “Важное сообщение”
            checkDialogMessageIsDisplayed(DIALOG_MESSAGE) //с текстом “Теперь ты автоматизатор”
        }
    }

    @Test
    fun closeDialogTest() {
        with(MainPage()) {
            pressShowDialogButton() //Нажать на кнопку Show Dialog
            checkDialogTitleIsDisplayed(DIALOG_TITLE) //Проверка что заголовок отображается
            checkDialogMessageIsDisplayed(DIALOG_MESSAGE) //Проверка что текст отображается
            pressBack() //Нажать системную кнопку “Назад”
            checkDialogTitleIsNotDisplayed() //Проверка что нет заголовка
            checkDialogMessageIsNotDisplayed() //Проверка нет текста сообщения
        }
    }

    private companion object {
        const val DIALOG_TITLE = "Важное сообщение"
        const val DIALOG_MESSAGE = "Теперь ты автоматизатор"
    }
}
