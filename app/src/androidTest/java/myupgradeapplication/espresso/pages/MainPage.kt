package myupgradeapplication.espresso.pages

import android.widget.LinearLayout
import androidx.appcompat.widget.DialogTitle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import ru.tinkoff.myupgradeapplication.R
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf

class MainPage {
    private val nextButton = withId(R.id.button_first)
    private val changeButton = withId(R.id.change_button)
    private val showDialog = withId(R.id.dialog_button)
    private val dialogTitle = allOf(
        instanceOf(DialogTitle::class.java),
        withParent(instanceOf(LinearLayout::class.java)))
    private val dialogMessage = withId(android.R.id.message)
    private val textOnPage = withId(R.id.textview_first)

    fun pressNextButton() {
        onView(nextButton)
            .perform(click())
    }

    fun pressChangeButton() {
        onView(changeButton)
            .perform(click())
    }

    fun pressShowDialogButton() {
        onView(showDialog)
            .perform(click())
    }

    fun checkDialogTitleIsDisplayed(title: String) {
        onView(dialogTitle)
            .check(matches(withText(title)))
    }

    fun checkDialogMessageIsDisplayed(message: String) {
        onView(dialogMessage)
            .check(matches(withText(message)))
    }

    fun checkDialogTitleIsNotDisplayed() {
        onView(dialogTitle)
            .check(doesNotExist())
    }

    fun checkDialogMessageIsNotDisplayed() {
        onView(dialogMessage)
            .check(doesNotExist())
    }

    fun checkTextOnPage(text: String) {
        onView(textOnPage)
            .check(matches(withText(text)))
    }
}
