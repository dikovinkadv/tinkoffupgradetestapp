package myupgradeapplication.espresso.pages

import android.widget.LinearLayout
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withHint
import com.google.android.material.textview.MaterialTextView
import ru.tinkoff.myupgradeapplication.R
import org.hamcrest.core.AllOf.allOf
import org.hamcrest.core.IsInstanceOf.instanceOf

class LoginPage {
    private val previousButton = withId(R.id.button_second)
    private val submitButton = withId(R.id.button_submit)
    private val loginField = withId(R.id.edittext_login)
    private val passwordFiled = withId(R.id.edittext_password)
    private val loginHint = withHint(R.string.login_hint)
    private val passwordHint = withHint(R.string.password_hint)
    private val snackBarTextView = allOf(
        instanceOf(MaterialTextView::class.java),
        withParent(instanceOf(LinearLayout::class.java))
    )

    fun pressPreviousButton() {
        onView(previousButton)
            .perform(click())
    }

    fun pressSubmitButton() {
        onView(submitButton)
            .perform(click())
    }

    fun enterLogin(loginValue: String) {
        onView(loginField)
            .perform(typeText(loginValue))
    }

    fun enterPassword(passwordValue: String) {
        onView(passwordFiled)
            .perform(typeText(passwordValue))
    }

    fun checkEmptyLoginField() {
        onView(loginField)
            .check(matches(loginHint))
    }

    fun checkEmptyPasswordField() {
        onView(passwordFiled)
            .check(matches(passwordHint))
    }

    fun checkTextOnSnackBar(textOnSnackBar: String) {
        onView(snackBarTextView)
            .check(matches(withText(textOnSnackBar)))
    }
}
