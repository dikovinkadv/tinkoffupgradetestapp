package myupgradeapplication.espresso

import org.junit.Test
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import myupgradeapplication.espresso.pages.MainPage
import myupgradeapplication.espresso.pages.LoginPage
import ru.tinkoff.myupgradeapplication.MainActivity

@RunWith(AndroidJUnit4::class)
class DisplayErrorMessagesTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun passwordFieldMustBeFilledMessageTest() {
        val loginValue = "Dikovinka"
        val textOnSnackBar = "Password field must be filled!"
        MainPage().pressNextButton() //Нажать на кнопку Next
        with(LoginPage()) {
            enterLogin(loginValue) //Ввести значение в поле login
            pressSubmitButton() //Нажать submit
            checkTextOnSnackBar(textOnSnackBar) //ОР: “Password field must be filled!”
        }
    }

    @Test
    fun loginFieldMustBeFilledMessageTest() {
        val passwordValue = "Parolchik"
        val textOnSnackBar = "Login field must be filled!"
        MainPage().pressNextButton() //Нажать на кнопку Next
        with(LoginPage()) {
            enterPassword(passwordValue) //Ввести значение в поле password
            pressSubmitButton() //Нажать submit
            checkTextOnSnackBar(textOnSnackBar) //ОР: “Login field must be filled!”
        }
    }

    @Test
    fun bothOfFieldsMustBeFilledMessageTest() {
        val textOnSnackBar = "Both of fields must be filled!"
        MainPage().pressNextButton() //Нажать на кнопку Next
        with(LoginPage()) {
            pressSubmitButton() //Нажать submit
            checkTextOnSnackBar(textOnSnackBar) //ОР: “Both of fields must be filled!”
        }
    }
}
