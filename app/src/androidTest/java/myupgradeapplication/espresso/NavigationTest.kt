package myupgradeapplication.espresso

import org.junit.Test
import org.junit.Rule
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.junit.rules.ActivityScenarioRule
import myupgradeapplication.espresso.pages.LoginPage
import myupgradeapplication.espresso.pages.MainPage
import ru.tinkoff.myupgradeapplication.MainActivity
import androidx.test.platform.app.InstrumentationRegistry
import ru.tinkoff.myupgradeapplication.R

@RunWith(AndroidJUnit4::class)
class NavigationTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun textPositionNotSavedTest() {
        val mainPage = MainPage()
        with(mainPage){
            pressChangeButton() //Нажать на кнопку Change
            checkTextOnPage(SECOND_TEXT) //В текстовом поле текст сменился
            pressNextButton() //Нажать на кнопку Next
        }
        with(LoginPage()) {
            pressPreviousButton() //Нажать на кнопку Previous
        }
        with(mainPage){
            checkTextOnPage(FIRST_TEXT) //ОР: В текстовом поле отображается текст по умолчанию
        }
    }

    @Test
    fun testLoginPasswordValuesNotSaved() {
        val loginValue = "Dikovinka"
        val passwordValue = "Parolchik"
        val mainPage = MainPage()
        val loginPage = LoginPage()
        with(mainPage){
            pressNextButton() //Нажать на кнопку Next
        }
        with(loginPage) {
            enterLogin(loginValue) //Заполнить поле Login
            enterPassword(passwordValue) //Заполнить поле Password
            pressPreviousButton() //Нажать на кнопку Previous
        }
        with(mainPage){
            pressNextButton() //Нажать на кнопку Next
        }
        with(loginPage) {
            checkEmptyLoginField() //ОР: Поле login пустое
            checkEmptyPasswordField() //ОР: Поле password пустое
        }
    }

    private companion object {
        val FIRST_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.first_text)
        val SECOND_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.second_text)
    }
}
