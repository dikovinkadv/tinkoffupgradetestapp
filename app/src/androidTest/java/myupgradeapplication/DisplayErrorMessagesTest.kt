package myupgradeapplication

import androidx.test.ext.junit.rules.ActivityScenarioRule
import myupgradeapplication.pages.MainPage
import myupgradeapplication.pages.LoginPage
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity

class DisplayErrorMessagesTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun passwordFieldMustBeFilledMessageTest() {
        val loginValue = "Dikovinka"
        val textOnSnackBar = "Password field must be filled!"
        with(MainPage()){
            pressNextButton()
        }
        with(LoginPage()) {
            enterLogin(loginValue)
            pressSubmitButton()
            checkTextOnSnackBar(textOnSnackBar)
        }
    }

    @Test
    fun loginFieldMustBeFilledMessageTest() {
        val passwordValue = "Parolchik"
        val textOnSnackBar = "Login field must be filled!"
        with(MainPage()){
            pressNextButton()
        }
        with(LoginPage()) {
            enterPassword(passwordValue)
            pressSubmitButton()
            checkTextOnSnackBar(textOnSnackBar)
        }
    }

    @Test
    fun bothOfFieldsMustBeFilledMessageTest() {
        val textOnSnackBar = "Both of fields must be filled!"
        with(MainPage()){
            pressNextButton()
        }
        with(LoginPage()) {
            pressSubmitButton()
            checkTextOnSnackBar(textOnSnackBar)
        }
    }
}
