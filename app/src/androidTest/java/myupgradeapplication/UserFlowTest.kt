package myupgradeapplication

import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test
import myupgradeapplication.pages.MainPage
import myupgradeapplication.pages.LoginPage
import ru.tinkoff.myupgradeapplication.MainActivity

//Пример теста из лекции
class UserFlowTest {
    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun enterLoginPasswordTest() {
        val loginValue = "Dikovinka"
        val passwordValue = "Parolchik"
        with(MainPage()){
            pressNextButton()
        }
        with(LoginPage()) {
            enterLogin(loginValue)
            enterPassword(passwordValue)
            pressSubmitButton()
            checkTextOnSnackBar("You enter login = $loginValue password = $passwordValue")
        }
    }
}
